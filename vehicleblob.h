#ifndef VEHICLE_BLOB_H
#define VEHICLE_BLOB_H

#include <opencv2/opencv.hpp>
//#include <QObject>
//#include <QPoint>
#include <frame.h>

class Frame;

/**
 * @file        vehicleblob.h
 * @author      Kabbaj Kaoutar
 * @version     1.0.0
 */

/*!
 * @ingroup     Detection
 * @class       VehicleBlob    vehicleblob.h
 * @brief      ce module décrit les véhicules détectés par le scope (x,y) du boiding box et le centroid
 */



class VehicleBlob
{

public:

    /*! @brief le constructeur VehicleBlob
     *
     */
    VehicleBlob();


    /*! @brief définir x,y du boinding box
     *  @param rectangle du véhicule détecté (le boinding box)
     */
    void SetScope (cv::Rect bbox);
    /*! @brief définir le centroid du blob
     *  @param x,y du centroid du blob
     */
    void SetCentroid(cv::Point centroid);

    /*! @brief obtenir le scope
     * @return rectangle du blob détecté
     */
    cv::Rect GetScope();
    /*! @brief obtenir le centroid
     * @return les coordonnées du centroid du blob
     */
    cv::Point GetCentroid();



    /*! @brief Obtenir le frame du blob
     * @return  pointeur frame du blob
     */
    Frame* GetFrame();

    /*! @brief définir le frame du blob
     *  @param pointeur sur le frame
     */
    void SetFrame(Frame *frame);

//signals:

//public slots:

private:
    /*! @brief rectangle du blob détecté
     *
     */
    cv::Rect bbox_;

    /*! @brief centroid du blob
     *
     */
    cv::Point centroid_;

    /*! @brief pointeur sur frame du blob
     *
     */
    Frame* frame_;
};

#endif // VEHICLE_BLOB_H
