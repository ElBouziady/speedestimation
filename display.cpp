#include "display.h"

using namespace std;
using namespace cv;


Display::Display()
{

}

Display::~Display()
{

}

bool Display::DisplayVehicles(vector<Vehicle>* vehicles,Frame *frame){
    frame->Lock("Display");

    if(!frame->IsSpeed()){
        cout<<"Display :: Speed Not Computed!!! "<<endl;
        frame->Unlock("Display");
        return false;
    }

    //cout<<"Drawing ... "<<endl;
    Mat image_mono, image_mono_raw;


    frame->GetImageInputMono().copyTo(image_mono);
    //frame->GetImageInputMono().copyTo(image_mono_raw);

    for (size_t i = 0; i < vehicles->size(); i++)
    {
        Rect vehicle_rect = (vehicles->at(i)).GetLastVehicleBlob().GetScope();
        Rect label_rect = Rect(vehicle_rect.x,vehicle_rect.y - vehicle_rect.height* 20/100, vehicle_rect.width, vehicle_rect.height* 20/100);
        Point label_point = Point(label_rect.x + label_rect.width * 5/100,label_rect.y + label_rect.height * 80/100);

        rectangle(image_mono, vehicle_rect, colors_[vehicles->at(i).GetId() % 9], 3, CV_AA);

        if (vehicles->at(i).GetCurrentSpeed() != -1){
            rectangle(image_mono, label_rect, colors_[(vehicles->at(i).GetId()+1) % 9], CV_FILLED);
            putText(image_mono, format("%.0f Km/h",vehicles->at(i).GetCurrentSpeed()), label_point, FONT_HERSHEY_PLAIN, label_rect.height/20.0, CV_RGB(0,0,0), 2);
          //  putText(image_mono, "LIGHT", label_point, FONT_HERSHEY_PLAIN, label_rect.height/20.0, CV_RGB(0,0,0), 2);

        }
        for (size_t j = 1; j < vehicles->at(i).GetVehicleBlobs().size() - 1; j++)
        {
            Point p1(vehicles->at(i).GetVehicleBlobs()[j].GetCentroid().x,vehicles->at(i).GetVehicleBlobs()[j].GetCentroid().y);
            Point p2(vehicles->at(i).GetVehicleBlobs()[j+1].GetCentroid().x,vehicles->at(i).GetVehicleBlobs()[j+1].GetCentroid().y);
            cv::line(image_mono, p1, p2, colors_[vehicles->at(i).GetId() % 9], 2, CV_AA);
        }
    }


    namedWindow("Display",WINDOW_NORMAL);
    resizeWindow("Display", 1000,600);
    imshow("Display",image_mono);
    //imwrite("/home/boukary/display.bmp",image_mono);
    waitKey(1);


    //frame->SetImageDisplay(image_mono);
    frame->SetState(DISPLAY);
    cout<<"Drawing Finished "<<endl;
    frame->Unlock("Display");

    //emit VehicleDisplayed(vehicles,frame);

    return true;
}

