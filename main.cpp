//#include <QCoreApplication>
#include <iostream>

#include "speedestimator.h"
#include "display.h"

using namespace std;

int main(int argc, char *argv[])
{
    //QCoreApplication a(argc, argv);

    cout<<"##### Speed Estimation Bloc (MoVITS)#####"<<endl;

    SpeedEstimator speed_estimator;
    Display display;
    std::vector<Vehicle> vehicles;
    Frame frame;

    while (true){
        if (!speed_estimator.LoadData(&vehicles,&frame))
            break;
        speed_estimator.EstimateSpeed(&vehicles,&frame);
        display.DisplayVehicles(&vehicles,&frame);
        vehicles.clear();
        frame = Frame();
    }

    cout<<"##### Process Finished #####"<<endl;

    //return a.exec();
    return 0;
}
