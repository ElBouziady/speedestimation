#include "frame.h"

using namespace cv;
using namespace std;

Frame::Frame()
{
    state_ = INVALID;
    //mutex_ = new QMutex;
    total_cpu_time_ = 0;
}

Mat Frame::GetImageInputMono(){
    return image_input_mono_;
}

Mat Frame::GetImagePreprocessedMono(){
    return image_preprocessed_mono_;
}

Mat Frame::GetImageInputStereo(){
    return image_input_stereo_;
}

Mat Frame::GetImagePreprocessedStereo(){
    return image_preprocessed_stereo_;
}

Mat Frame::GetImageDepth(){
    return image_depth_;
}

vector<VehicleBlob> Frame::GetVehiclesDetected(){
    return vehicles_detected_;
}

Mat Frame::GetImageDisplay(){
    return image_display_;
}

void Frame::SetImageInputMono(Mat image_input_mono){
    if(image_input_mono.empty()){
        cout<<"Frame :: Image Empty !!! "<<endl;
        return;
    }
    if ((state_ != INVALID) && (state_ != ACQUIRE))
        return;
    image_input_mono_ = image_input_mono;
    is_stereo_ = false;
}

void Frame::SetImagePreprocessedMono(Mat image_preprocessed_mono){
    if(image_preprocessed_mono.empty()){
        cout<<"Frame :: Image Empty !!! "<<endl;
        return;
    }
    if (state_ != ACQUIRE)
        return;
    image_preprocessed_mono_ = image_preprocessed_mono;
}

void Frame::SetImageInputStereo(Mat image_input_mono, Mat image_input_stereo){
    if(image_input_mono.empty() || image_input_stereo.empty()){
        cout<<"Frame :: Image Stereo Empty !!! "<<endl;
        return;
    }
    if ((state_ != INVALID) && (state_ != ACQUIRE))
        return;
    image_input_mono_ = image_input_mono;
    image_input_stereo_ = image_input_stereo;
    is_stereo_ = true;
}

void Frame::SetImagePreprocessedStereo(Mat image_preprocessed_mono, Mat image_preprocessed_stereo){
    if(image_preprocessed_mono.empty() || image_preprocessed_stereo.empty()){
        cout<<"Frame :: Image Stereo Empty !!! "<<endl;
        return;
    }
    if (!IsAcquired())
        return;
    image_preprocessed_mono_ = image_preprocessed_mono;
    image_preprocessed_stereo_ = image_preprocessed_stereo;
}

void Frame::SetImageDepth(Mat image_depth){
    if(image_depth.empty()){
        cout<<"Frame :: Image Depth Empty !!! "<<endl;
        return;
    }
    if (!IsPreprocessed())
        return;
    image_depth_ = image_depth;
}

vector<Mat> Frame::GetVehiclesDetectedDepth(){
    return vehicles_detected_depth_;
}

void Frame::SetVehiclesDetectedDepth(vector<Mat> &vehicles_detected_depth){
    if (!IsDetected())
        return;
    vehicles_detected_depth_ = vehicles_detected_depth;
}

bool Frame::IsStereo()
{
    return is_stereo_;
}

void Frame::SetStereo(bool is_stereo)
{
    is_stereo_ = is_stereo;
}

void Frame::SetVehiclesDetected(vector<VehicleBlob> vehicles_detected){
    if (!IsPreprocessed())
        return;
    vehicles_detected_ = vehicles_detected;
}

void Frame::SetImageDisplay(Mat image_display){
    if(image_display.empty()){
        cout<<"Frame :: Image Empty !!! "<<endl;
        return;
    }
    if (!IsTracked() && !IsSpeed())
        return;
    image_display_ = image_display;
}

void Frame::SetState(int state){
    state_ = state;
}

void Frame::Lock(string bloc_name){
    if (bloc_name != "Updating Tracks"){
        cout<<"Start Timer In "<<bloc_name<<endl;
        start_time_ = cv::getTickCount();
    }
    //while(!mutex_->tryLock());
    is_locked_ = true;
}

void Frame::Unlock(string bloc_name){
    //mutex_->unlock();
    is_locked_ = false;
    if (bloc_name == "Updating Tracks")
        return;
    end_time_ = cv::getTickCount();
    double cpu_time = (end_time_ - start_time_)*1000/getTickFrequency();
    total_cpu_time_ += cpu_time;
    ofstream stat_file("timer_values.csv",ofstream::out | ofstream::app);
    stat_file << cpu_time << ",";
    cout<<"CPU Time : "<<cpu_time<<" ms"<<endl;
    cout<<"End Timer In "<<bloc_name<<endl;
    if (IsDisplayed()){
        cout<<endl<<"Total CPU Time : "<<total_cpu_time_<<" ms"<<endl<<"///////////////////////////////"<<endl<<endl;
        stat_file << "," << total_cpu_time_ << "\n";
    }
    stat_file.close();
}

bool Frame::IsLocked(){
    return is_locked_;
}

bool Frame::IsInvalid(){
    return (state_ == INVALID);
}

bool Frame::IsAcquired(){
    return (state_ != INVALID);
}

bool Frame::IsPreprocessed(){
    return (state_ != INVALID) && (state_ != ACQUIRE);
}

bool Frame::IsDepth(){
    return (is_stereo_ && (!vehicles_detected_depth_.empty() || !image_depth_.empty()));
}

bool Frame::IsDetected(){
    return (state_ != INVALID) && (state_ != ACQUIRE) && (state_ != PREPROCESS);
}

bool Frame::IsClassified(){
    return (state_ != INVALID) && (state_ != ACQUIRE) && (state_ != PREPROCESS) && (state_ != DETECT) && (state_ != TRACK);
}

bool Frame::IsTracked(){
    return (state_ != INVALID) && (state_ != ACQUIRE) && (state_ != PREPROCESS) && (state_ != DETECT);
}

bool Frame::IsRecognized(){
    return (state_ != INVALID) && (state_ != ACQUIRE) && (state_ != PREPROCESS) && (state_ != DETECT) && (state_ != TRACK) && (state_ != CLASSIFY);
}

bool Frame::IsSpeed(){
    return (state_ != INVALID) && (state_ != ACQUIRE) && (state_ != PREPROCESS) && (state_ != DETECT) && (state_ != TRACK);
}

bool Frame::IsDisplayed(){
    return (state_ == DISPLAY);
}

double Frame::GetTotalCPUTime(){
    return total_cpu_time_;
}
