#include "speedestimator.h"

using namespace std;
using namespace cv;

SpeedEstimator::SpeedEstimator(/*QObject *parent) : QObject(parent*/)
{
    /*
    dec = 10
    x0 = 1400 - 1024
    pitch = 13.1 - 6.1 - 11
    fps = 31.94 - 32
    video de dos/ video4/input1.avi
    B = 0.4
    f = 0.05
    ouverture = 11.84
*/

    index_frame_ = 1;

}

float SpeedEstimator::ConvertDisparity2Distance(float disparity){

    float distance;
    if (true){
        /// methode stagiare 2014
        float S = ((0.0000074*image_width_)/2)/focal_distance_;
        distance = baseline_ * image_width_ / (2 * S * disparity);
        distance /= (cos(pitch_/180)*M_PI);
    }
    else {
        distance = baseline_ * image_width_ / (2 * tan(view_angle_camera_/2) * disparity);
        distance /= (cos(pitch_/180)*M_PI);
    }

    return distance;
}

bool SpeedEstimator::ComputeDistanceStereo(Vehicle* vehicle){
    if (vehicle == NULL)
        return false;

    Mat depth_map = vehicle->GetDepthImage();

    if (depth_map.empty())
        return false;

    float moy_disparity = 0;
    for (int i = 0; i < depth_map.rows; ++i) {
        for (int j = 0; j < depth_map.cols; ++j) {
            moy_disparity += depth_map.at<int>(i,j);
        }
    }
    moy_disparity /= (depth_map.rows * depth_map.cols);

    vehicle->AddDistance(ConvertDisparity2Distance(moy_disparity));

    return true;
}

bool SpeedEstimator::ComputeDistanceStereo(Vehicle* vehicle,Frame* frame){

    //Declaration

    //cout<<"Declaration ...."<<endl;

    int minHessian = 400;
    SurfFeatureDetector detector( minHessian );
    SurfDescriptorExtractor extractor;
    //BFMatcher matcher(NORM_L2);
    FlannBasedMatcher matcher;
    std::vector<KeyPoint> keypoints_mono,keypoints_stereo;
    Mat descriptors_mono, descriptors_stereo;
    std::vector< DMatch > matches;
    std::vector< DMatch > good_matches;
    Rect vehicle_rect = vehicle->GetLastVehicleBlob().GetScope();

    Rect vehicle_rect_mono = vehicle_rect;
    Rect vehicle_rect_stereo = vehicle_rect;
    int margin = min(vehicle_rect_mono.width,vehicle_rect_mono.height)*0.3;
    Mat image_mono,image_stereo;

    frame->GetImageInputMono().copyTo(image_mono);
    frame->GetImageInputStereo().copyTo(image_stereo);


    // ROIs

    //cout<<"ROIs ...."<<endl;

    vehicle_rect_mono.x += margin ;
    vehicle_rect_mono.y += margin;
    vehicle_rect_mono.width -= 2*margin;
    vehicle_rect_mono.height -= 2*margin;

    vehicle_rect_stereo = vehicle_rect_mono;
    vehicle_rect_stereo.x += shift_x_;
    vehicle_rect_stereo.y += shift_y_;

    Rect image_rect = Rect(0,0,image_mono.cols,image_mono.rows);

    vehicle_rect_mono = image_rect & vehicle_rect_mono;
    vehicle_rect_stereo = image_rect & vehicle_rect_stereo;

    //cout<<"Mono Rect : "<<vehicle_rect_mono<<endl;
    //cout<<"Stereo Rect : "<<vehicle_rect_stereo<<endl;

    // Detect, describe and match keypoints

    //cout<<"Detect, describe and match keypoints ...."<<endl;

    Mat mask_mono = Mat::zeros(image_mono.size(), CV_8U);  // type of mask is CV_8U
    Mat roi_mono(mask_mono, vehicle_rect_mono);
    roi_mono = Scalar(255, 255, 255);

    Mat mask_stereo = Mat::zeros(image_stereo.size(), CV_8U);
    Mat roi_stereo(mask_stereo, vehicle_rect_stereo);
    roi_stereo = Scalar(255, 255, 255);

    detector.detect( image_mono, keypoints_mono , mask_mono);
    extractor.compute( image_mono, keypoints_mono, descriptors_mono );

    detector.detect( image_stereo, keypoints_stereo , mask_stereo);
    extractor.compute( image_stereo, keypoints_stereo, descriptors_stereo );

    if (descriptors_mono.empty() || descriptors_stereo.empty() || (descriptors_mono.size().width != descriptors_stereo.size().width))
        return false;

    matcher.match( descriptors_mono, descriptors_stereo, matches );


    //Compute filtering parameters

    //cout<<"Compute filtering parameters ...."<<endl;

    float moy_slope_old = 0;
    float moy_slope_new = 0;
    float moy_disparity_x_old = 0;
    float moy_disparity_x_new = 0;
    float moy_disparity_y_old = 0;
    float moy_disparity_y_new = 0;

    for (int i = 0; i < matches.size(); ++i) {
        float slope  = (keypoints_mono[matches[i].queryIdx].pt.y - keypoints_stereo[matches[i].trainIdx].pt.y) / (keypoints_mono[matches[i].queryIdx].pt.x - keypoints_stereo[matches[i].trainIdx].pt.x);
        float disparity_x = fabs(keypoints_mono[matches[i].queryIdx].pt.x - keypoints_stereo[matches[i].trainIdx].pt.x);
        float disparity_y = fabs(keypoints_mono[matches[i].queryIdx].pt.y - keypoints_stereo[matches[i].trainIdx].pt.y);
        moy_slope_old += slope;
        moy_disparity_x_old += disparity_x;
        moy_disparity_y_old += disparity_y;
    }
    if (!matches.empty()){
        moy_slope_old /= matches.size();
        moy_disparity_x_old /= matches.size();
        moy_disparity_y_old /= matches.size();
    }
    int matches_old_size = matches.size();

    double max_dist = 0; double min_dist = 100;

    for( int i = 0; i < descriptors_mono.rows; i++ )
    { double dist = matches[i].distance;
        if( dist < min_dist ) min_dist = dist;
        if( dist > max_dist ) max_dist = dist;
    }


    // Filter

    //cout<<"Filter ...."<<endl;

    for( int i = 0; i < matches.size(); i++ )
    {
        if( matches[i].distance <= max(2*min_dist, 0.02) )
        { good_matches.push_back( matches[i]); }
    }


    for (int i = 0; i < good_matches.size(); ++i) {
        float slope  = (keypoints_mono[good_matches[i].queryIdx].pt.y - keypoints_stereo[good_matches[i].trainIdx].pt.y) / (keypoints_mono[good_matches[i].queryIdx].pt.x - keypoints_stereo[good_matches[i].trainIdx].pt.x);
        float disparity_x = fabs(keypoints_mono[good_matches[i].queryIdx].pt.x - keypoints_stereo[good_matches[i].trainIdx].pt.x);
        float disparity_y = fabs(keypoints_mono[good_matches[i].queryIdx].pt.y - keypoints_stereo[good_matches[i].trainIdx].pt.y);
        // if (fabs(slope)>fabs(moy_slope_old))
        //   good_matches.erase(good_matches.begin()+j,good_matches.begin()+j+1);
        //cout<<"Slope : "<<slope<<endl;

        if (slope_max_ < slope || slope < slope_min_){
            good_matches.erase(good_matches.begin()+i,good_matches.begin()+i+1);
            i--;
        }
    }

    for (int i = 0; i < good_matches.size(); ++i) {
        float slope  = (keypoints_mono[good_matches[i].queryIdx].pt.y - keypoints_stereo[good_matches[i].trainIdx].pt.y) / (keypoints_mono[good_matches[i].queryIdx].pt.x - keypoints_stereo[good_matches[i].trainIdx].pt.x);
        float disparity_x = fabs(keypoints_mono[good_matches[i].queryIdx].pt.x - keypoints_stereo[good_matches[i].trainIdx].pt.x);
        float disparity_y = fabs(keypoints_mono[good_matches[i].queryIdx].pt.y - keypoints_stereo[good_matches[i].trainIdx].pt.y);
        moy_slope_new += slope;
        moy_disparity_x_new += disparity_x;
        moy_disparity_y_new += disparity_y;
    }
    if (!good_matches.empty()){
        moy_slope_new /= good_matches.size();
        moy_disparity_x_new /= good_matches.size();
        moy_disparity_y_new /= good_matches.size();
    }


    /*
    // Draw

    cout<<"Draw ...."<<endl;

    cv::Scalar colors[9] = { cv::Scalar(255, 0, 0), cv::Scalar(0, 255, 0), cv::Scalar(0, 0, 255), cv::Scalar(255, 255, 0), cv::Scalar(0, 255, 255), cv::Scalar(255, 0, 255), cv::Scalar(255, 127, 255), cv::Scalar(127, 0, 255), cv::Scalar(127, 0, 127) };

    rectangle(image_mono, vehicle_rect_mono, Scalar(0,0,255), 2, CV_AA);
    rectangle(image_stereo, vehicle_rect_stereo, Scalar(0,0,255), 2, CV_AA);

    if (keypoints_mono.size()>0)
        drawKeypoints( image_mono, keypoints_mono, image_mono, Scalar::all(-1), DrawMatchesFlags::DEFAULT );

    if (keypoints_stereo.size()>0)
        drawKeypoints( image_stereo, keypoints_stereo, image_stereo, Scalar::all(-1), DrawMatchesFlags::DEFAULT );


    Mat img_matches;
    drawMatches( image_mono, keypoints_mono, image_stereo, keypoints_stereo, good_matches, img_matches );

    namedWindow("matches",WINDOW_NORMAL);
    resizeWindow("matches", 1000,600);
    imshow("matches",img_matches);
    //imwrite("/home/boukary/matches.bmp",img_matches);

    for (int i = 0; i < good_matches.size(); ++i) {
        circle(image_mono,keypoints_mono[good_matches[i].queryIdx].pt, 10, colors[i % 9]);
        circle(image_mono,keypoints_stereo[good_matches[i].trainIdx].pt, 10, colors[i % 9]);
        line(image_mono, keypoints_mono[good_matches[i].queryIdx].pt, keypoints_stereo[good_matches[i].trainIdx].pt, colors[i % 9], 2, CV_AA);
    }

    putText(image_mono, format("Disparity X = %.2f ===> %.2f",moy_disparity_x_old,moy_disparity_x_new), Point(20,20), FONT_HERSHEY_PLAIN, 1, CV_RGB(255,255,255), 1.9);
    putText(image_mono, format("Disparity Y = %.2f ===> %.2f",moy_disparity_y_old,moy_disparity_y_new), Point(20,40), FONT_HERSHEY_PLAIN, 1, CV_RGB(255,255,255), 1.9);
    putText(image_mono, format("Slope = %.2f ===> %.2f",moy_slope_old,moy_slope_new), Point(20,60), FONT_HERSHEY_PLAIN, 1, CV_RGB(255,255,255), 1.9);
    putText(image_mono, format("Matches %d ====> %d",matches_old_size,good_matches.size()), Point(20,80), FONT_HERSHEY_PLAIN, 1, CV_RGB(255,255,255), 1.9);

    cout<<"Width : "<<image_mono.cols<<"  Height"<<image_mono.rows<<endl;
    cout<<"Mono : "<<vehicle_rect_mono<<endl;
    cout<<"Stereo : "<<vehicle_rect_stereo<<endl;

    namedWindow("Speed Display Mono",WINDOW_NORMAL);
    resizeWindow("Speed Display Mono", 800,600);

    namedWindow("Speed Display Stereo",WINDOW_NORMAL);
    resizeWindow("Speed Display Stereo", 800,600);

    imshow("Speed Display Mono",image_mono);
    imshow("Speed Display Stereo",image_stereo);

    waitKey(0);
*/

    if (moy_disparity_x_new == 0)
        return false;

    double distance = ConvertDisparity2Distance(moy_disparity_x_new);
    if (!vehicle->GetDistanceList().empty())
        distance = 0.3 * distance + 0.7 * vehicle->GetCurrentDistance();

    vehicle->AddDistance(distance);

    return true;
}

bool SpeedEstimator::EstimateSpeed(std::vector<Vehicle>* vehicles,Frame* frame){

    frame->Lock("Speed Estimation");

    frame->SetState(TRACK);

    if(!frame->IsTracked()){
        cout<<"SpeedEstimator :: Image Not Tracked !!! "<<endl;
        frame->Unlock("Speed Estimation");
        return false;
    }

    index_frame_++;

    /*for (int i = 0; i < vehicles->size(); ++i) {

        Rect vehicle_rect = (vehicles->at(i)).GetLastVehicleBlob().GetScope();
        //cout<<"------------------- Vehicle "<<i<<" Rect :  "<<vehicle_rect<<endl;
    }*/

    if (frame->IsStereo()){
        /*
    if(!frame->IsDepth()){
        cout<<"SpeedEstimator :: Depth Map Not Generated !!! "<<endl;
        frame->Unlock("Speed Estimation");
        frame->Lock("Speed Estimation");
    }
*/
        cout<<"Estimating Speed Stereo... "<<endl;

        ///////


        /*
    if (!frame->GetImageDepth().empty()){
        Mat image_depth = frame->GetImageDepth();

        for (int i = 0; i < vehicles->size() ; ++i) {
            cout<<"ss"<<i<<endl;
            vehicles->at(i).SetDepthImage(image_depth(vehicles->at(i).GetLastVehicleBlob().GetScope()));
        }
    }

    if (frame->GetVehiclesDetectedDepth().size()>0){
        vector<VehicleBlob> vehicles_detected = frame->GetVehiclesDetected();
        vector<Mat> vehicles_detected_depth = frame->GetVehiclesDetectedDepth();

        for (int i = 0; i < vehicles_detected.size() ; ++i) {
            for (int j = 0; j < vehicles->size() ; ++j){
                Rect r1 = vehicles_detected[i].GetScope();
                Rect r2 = vehicles->at(j).GetLastVehicleBlob().GetScope();
                if ((abs(r1.x-r2.x) < 10) && (abs(r1.y-r2.y) < 10) && (abs(r1.width-r2.width) < 10) && (abs(r1.height-r2.height) < 10)){
                    vehicles->at(j).SetDepthImage(vehicles_detected_depth[i]);
                }
            }
        }

    }
*/

        for (int i = 0; i < vehicles->size() ; ++i) {
            //ComputeDistanceStereo(&(vehicles->at(i)));
            ComputeDistanceStereo(&(vehicles->at(i)),frame);
            //break;
        }

        int fps = 32;
        for (int i = 0; i < vehicles->size(); ++i) {
            float moy_slope = 0;
            int size = vehicles->at(i).GetDistanceList().size();
            if (size<scope_)
                continue;

            ///// Moy Speed
            //if (!vehicles->at(i).GetSpeedList().empty())
            //    continue;
            /////



            for (int j = 0; j < scope_ - 1; ++j) {
                moy_slope += (vehicles->at(i).GetDistanceList().at(size - j - 1) - vehicles->at(i).GetDistanceList().at(size - j - 2));
            }
            moy_slope/= scope_;

            //double distance_par = fabs(vehicles->at(i).GetCurrentDistance() - vehicles->at(i).GetDistanceList().at(vehicles->at(i).GetDistanceList().size()-2));
            float speed = fabs(moy_slope)*fps*3.6;

            if (!vehicles->at(i).GetSpeedList().empty())
                speed = 0.5 * speed + 0.5 * vehicles->at(i).GetCurrentSpeed();

            vehicles->at(i).AddSpeed(speed);

            //vehicles->at(i).AddSpeed(0);
            //break;
        }
    }
    /////

    frame->SetState(SPEED);
    frame->Unlock("Speed Estimation");



    cout<<"Estimating Speed Finished "<<endl<<endl;
    //emit SpeedEstimated(vehicles,frame);
    return true;
}

void SpeedEstimator::SetConfig(ConfigSpeedEstimation config)
{
    baseline_= config.baseline;
    focal_distance_=config.focal_distance;
    pitch_=config.pitch;
    view_angle_camera_=config.view_angle_camera;
    image_width_=config.image_width;
    slope_min_=config.slope_min;
    slope_max_=config.slope_max;
    shift_x_=config.shift_x;
    shift_y_=config.shift_y;
    scope_=config.scope;
}



bool SpeedEstimator::SaveData(std::vector<Vehicle>* vehicles,Frame* frame){


    cout<<"Saving Data Started "<<endl;

    FileStorage fs("Input_Data_Speed_Estimation_Bloc.xml", FileStorage::APPEND);

    if(!fs.isOpened())
        return false;


    fs << format("Frame%05d",index_frame_) << "{";
    fs << "Num" << index_frame_;

    fs << "Vehicles" << "{";
    fs << "Size" << (int)vehicles->size();

    for (int i = 0; i < vehicles->size(); ++i) {

        fs << format("Vehicle%d",i) << "{";
        fs << "Id" << (int)vehicles->at(i).GetId();

        fs << "Blobs" << "{";
        fs << "Size" << (int)vehicles->at(i).GetVehicleBlobs().size();
        for (int j = 0; j < vehicles->at(i).GetVehicleBlobs().size(); ++j) {
            fs << format("Blob%d",j) << "{";
            fs << "Scope"<<vehicles->at(i).GetVehicleBlobs().at(j).GetScope();
            Point centroid = vehicles->at(i).GetVehicleBlobs().at(j).GetCentroid();
            fs << "Centroid"<<"{";
            fs << "x" << centroid.x;
            fs << "y" << centroid.y;
            fs << "}";
            fs << "}";
        }

        fs << "}";
        fs << "DistanceList"  << vehicles->at(i).GetDistanceList();
        fs << "SpeedList"  << vehicles->at(i).GetSpeedList();
        fs << "}";
    }
    fs << "}";

    fs << "}";

    fs.release();

    cout<<"Saving Data Finished "<<endl<<endl;

    return true;
}

bool SpeedEstimator::LoadData(std::vector<Vehicle>* vehicles,Frame* frame){

    cout<<"Loading Data Started "<<endl;

    frame->Lock("Loading Data");

    frame->SetState(ACQUIRE);

    FileStorage fs("Input(Renault Captur).xml", FileStorage::READ);
    //FileStorage fs("Input(Skoda Rapid).xml", FileStorage::READ);
    //FileStorage fs("Input(Toyota Auris).xml", FileStorage::READ);

    if(!fs.isOpened())
        return false;

    int num = (int)fs[format("Frame%05d",index_frame_)]["Num"];
    if (num == 0){
        fs.release();
        return false;
    }
    // add shift number dataset
    num += 485;
    //num += 857;
    //num += 1159;

    cout<<format("image%05d.bmp",num)<<endl;

    Mat input_mono = imread(format("Dataset/Renault Captur/1/image%05d.bmp",num));
    //Mat input_mono = imread(format("Dataset/Skoda Rapid/1/image%05d.bmp",num));
    //Mat input_mono = imread(format("Dataset/Toyota Auris/1/image%05d.bmp",num));
    cvtColor( input_mono, input_mono, CV_BGR2GRAY );
    cvtColor( input_mono, input_mono, CV_BayerGB2BGR );

    Mat input_stereo = imread(format("Dataset/Renault Captur/2/image%05d.bmp",num));
    //Mat input_stereo = imread(format("Dataset/Skoda Rapid/2/image%05d.bmp",num));
    //Mat input_stereo = imread(format("Dataset/Toyota Auris/2/image%05d.bmp",num));
    cvtColor( input_stereo, input_stereo, CV_BGR2GRAY );
    cvtColor( input_stereo, input_stereo, CV_BayerGB2BGR );
    frame->SetImageInputStereo(input_mono,input_stereo);

    Mat image_preprocessed_mono,image_preprocessed_stereo;
    input_mono.copyTo(image_preprocessed_mono);
    input_stereo.copyTo(image_preprocessed_stereo);

    cvtColor( image_preprocessed_mono, image_preprocessed_mono, CV_BGR2GRAY );
    cvtColor( image_preprocessed_stereo, image_preprocessed_stereo, CV_BGR2GRAY );
    frame->SetImagePreprocessedStereo(image_preprocessed_mono,image_preprocessed_stereo);


    FileNode fn_vehicles = fs[format("Frame%05d",index_frame_)][format("Vehicles",index_frame_)];
    int vehicles_size = fn_vehicles["Size"];

    for (int i = 0; i < vehicles_size; ++i) {

        Vehicle v;
        v.SetId((int)fn_vehicles[format("Vehicle%d",i)]["Id"]);
        cout<<" Vehicle ID : "<<v.GetId()<<endl;

        FileNode fn_blobs = fn_vehicles[format("Vehicle%d",i)]["Blobs"];
        int blobs_size = fn_blobs["Size"];

        for (int j = 0; j < blobs_size; ++j) {
            VehicleBlob b;
            std::vector<int> rect;
            fn_blobs[format("Blob%d",j)]["Scope"] >> rect;
            b.SetScope(Rect(rect[0], rect[1], rect[2], rect[3]));
            b.SetCentroid(Point(fn_blobs[format("Blob%d",j)]["Centroid"]["x"],fn_blobs[format("Blob%d",j)]["Centroid"]["y"]));

            v.AddVehicleBlob(b);
        }

        std::vector<float> distance_list;
        std::vector<float> speed_list;
        fn_vehicles[format("Vehicle%d",i)]["DistanceList"] >> distance_list;
        fn_vehicles[format("Vehicle%d",i)]["SpeedList"] >> speed_list;
        v.SetDistanceList(distance_list);
        v.SetSpeedList(speed_list);

        vehicles->push_back(v);
    }

    /*imshow("Mono",input_mono);
    imshow("Stereo",input_stereo);

    waitKey(1);*/

    fs.release();

    frame->Unlock("Loading Data");

    cout<<"Loading Data Finished "<<endl<<endl;

    return true;
}
