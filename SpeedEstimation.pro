QT += core
QT -= gui

CONFIG += c++11

TARGET = SpeedEstimation
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    frame.cpp \
    speedestimator.cpp \
    vehicle.cpp \
    vehicleblob.cpp \
    display.cpp

DISTFILES += \
    SpeedEstimation.pro.user

HEADERS += \
    frame.h \
    speedestimator.h \
    vehicle.h \
    vehicleblob.h \
    display.h


unix {
LIBS += `pkg-config --libs opencv`
INCLUDEPATH += '/usr/local/include'
}
