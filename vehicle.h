#ifndef VEHICLE_H
#define VEHICLE_H

#include <opencv2/opencv.hpp>
#include <vehicleblob.h>

/*! @brief une énumération indiquant le type de la classe du véhicule détecté si elle vaut 0 c'est que le véhicule est léger si c'est 1 c'est un véhicule lourd
 *
 */
enum VehicleClass
{
    LIGHT = 0,
    HEAVY1 = 1,
    HEAVY2 = 2
};
/**
 * @file        vehicle.h
 * @author      EL Bouziady Abderrahim
 * @version     1.0.0
 */
/*!
 * @ingroup     Buffer
 * @class       Vehicle    vehicle.h
 * @brief Permet de décrire le véhicules détecté (id, vitesse, couleur, classe)
 */

class Vehicle
{
public:
    /*! @brief le constructeur Vehicle
     *
     */
    Vehicle();

    /*! @brief attribué un id au véhicule détécté
     *  @param identifiant
     */
    void SetId(size_t id);
    /*! @brief ajouter véhicule détécté de type VehicleBlob (pour obtenir centroid et scop)
     *  @param véhicule de type VehicleBlob
     */
    void AddVehicleBlob(VehicleBlob blob);

    /*! @brief ajouter liste des véhicules détéctés de type VehicleBlob (pour obtenir centroid et scop)
     *  @param véhicules de type VehicleBlob
     */
    void SetVehicleBlobs(std::vector<VehicleBlob> blobs);

    /*! @brief définir les distances de la véhicule détecté
     *  @param liste des distances de type double
     */
    void SetDistanceList(std::vector<float> distance_list);

    /*! @brief définir la distance actuelle de la véhicule détecté - camera
     *  @param distance actuelle de type double
     */
    void AddDistance(float distance);

    /*! @brief définir les vitesses de la véhicule détecté
     *  @param liste des vitesse de type double
     */
    void SetSpeedList(std::vector<float> speed_list);

    /*! @brief définir la vitesse actuelle de la véhicule détecté
     *  @param vitesse actuelle de type double
     */
    void AddSpeed(float speed);

    /*! @brief définir la couleur de la véhicule détecté
     *  @param couleur de type scalar
     */
    void SetColor(std::string color);
    /*! @brief définir la class de la véhicule détecté
     *  @param la classe de véhicule (soit lourd ou léger)
     */
    void SetClass(VehicleClass class_);

    /*! @brief définir la plaque d'immatriculation de la véhicule détecté
     *  @param la plaque d'immatriculation de véhicule
     */
    void SetLP(std::string license_plate);

    /*! @brief définir la carte du profondeur de la véhicule détecté
     *  @param la carte du profondeur
     */
    void SetDepthImage(cv::Mat depth_image);

    /*! @brief avoir l'identifient du véhicule détécté
     *  @return retourne un identifiant de type size_t
     */

    size_t GetId();

    /*! @brief obtenir les positions du véhicule détécté
     *  @return un vecteur de blobs (positions)
     */
    std::vector<VehicleBlob> GetVehicleBlobs();

    /*! @brief obtenir la derniere position du véhicule détecté
     *  @return retourne un blob
     */
    VehicleBlob GetLastVehicleBlob();

    /*! @brief obtenir les distances du véhicule détécté
     *  @return liste des distances
     */
    std::vector<float> GetDistanceList();

    /*! @brief obtenir les vitesses du véhcule détécté
     *  @return liste des vitesses
     */
    std::vector<float> GetSpeedList();

    /*! @brief obtenir la distance actuelle du véhcule détécté
     *  @return la valeur de la distance actuelle (un double)
     */
    float GetCurrentDistance();

    /*! @brief obtenir la vitesse actuelle du véhcule détécté
     *  @return la valeur de la vitesse actuelle (un double)
     */
    float GetCurrentSpeed();

    /*! @brief obtenir la couleur du vhéicule détécté
     *  @return la couleur (un scalar)
     */
    std::string GetColor();

    /*! @brief obtenir la classe du véhicule (lourd ou léger)
     *  @return retourne la classe du véhicule détecté (0 ou 1)
     */
    VehicleClass GetClass();

    /*! @brief obtenir la plaque d'immatriculation du véhicule
     *  @return retourne plaque d'immatriculation du véhicule détecté
     */
    std::string GetLP();

    /*! @brief obtenir la carte du profondeur du véhicule
     *  @return retourne la carte du profondeur du véhicule détecté
     */
    cv::Mat GetDepthImage();

    bool IsClassified();

    bool IsRecognizedColor();
    bool IsRecognizedLP();

//signals:

//public slots:

private:
    /*!
     * @brief l'identifiant du véhicule détecté
     */
    size_t id_;
    /*!
     * @brief vecteur de blobs
     */
    std::vector<VehicleBlob> blobs_;

    /*!
     * @brief les distances du véhicule détecté
     */
    std::vector<float> distance_list_;

    /*!
     * @brief les vitesses du véhicule détecté
     */
    std::vector<float> speed_list_;

    /*!
     * @brief la couleur du véhicule détecté
     */
    std::string color_;
    /*!
     * @brief la classe du véhicule détecté
     */
    VehicleClass class_;

    /*!
     * @brief la plaque d'immatriculation du véhicule détecté
     */
    std::string license_plate_;

    /*!
     * @brief la carte du profondeur du véhicule détecté
     */
    cv::Mat depth_image_;

    bool is_classified_;
};

#endif // VEHICLE_H
