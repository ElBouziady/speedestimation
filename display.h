﻿#ifndef Display_H
#define Display_H

#include <iostream>
#include <iomanip>
#include <opencv2/opencv.hpp>
//#include <QObject>
#include <frame.h>
#include <vehicle.h>

using namespace std;
using namespace cv;

/**
 * @file        display.h
 * @author      El Bouziady Abderrahim
 * @version     1.0.0
 */

/*! @defgroup Display Display : Le module d'affichage
 * @brief Le module qui gére la partie affichage.
 */

/*!
 * @ingroup     Display
 * @class       Display    display.h
 * @brief Permet de contrôler l'affichage du systéme
 */
class Display //: public QObject
{
    //Q_OBJECT
public:

    /*!
     * @brief Constructeur par défaut de la classe Display
     */
    Display();

    /*!
     * @brief Destructeur par défaut de la classe Display
     */
    ~Display();

    /*!
     * @brief Permet de lancer un thread de l'affichage
     *//*
    void run();

    void SetFrame(Frame *frame);

    void SetDisplayInfo(DisplayInfo *display_info);*/

//public slots :

    bool DisplayVehicles(std::vector<Vehicle>* vehicles,Frame *frame);

    /*!
     * @brief Permet d'afficher le fps à l'écran
     * @param image : une image surlaquelle le fps va être affiché
     * @param fps : un reel qui contient le fps
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    // bool DrawFPS(Mat *image, DisplayInfo *display_info);

    /*!
     * @brief Permet d'afficher la date à l'écran
     * @param image : une image surlaquelle la date va être affiché
     * @param date : une chaîne de caractére qui contient la date
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    // bool DrawDate(Mat *image, DisplayInfo *display_info);

    /*!
     * @brief Permet d'afficher le pourcentage à l'écran
     * @param image : une image surlaquelle le pourcentage va être affiché
     * @param percentage : un entier qui contient le pourcentage
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    //bool DrawPercentage(Mat *image, DisplayInfo *display_info);

    /*!
     * @brief Permet d'afficher la zone d'intérêt à l'écran
     * @param image : une image surlaquelle la zone d'intérêt va être affiché
     * @param state : un entier indiquant l'état actuel du systéme
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    //bool DrawReference(Mat *image, DisplayInfo *display_info);

    /*!
     * @brief Permet d'afficher le logo à l'écran
     * @param image : une image surlaquelle le logo va être affiché
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */
    //bool DrawLogo(Mat *image, DisplayInfo *display_info);

    /*!
     * @brief Permet de faire la mise à jour du resultat de reconnaissance à l'écran
     * @param image : une image surlaquelle le resultat de reconnaissance va être affiché
     * @param state : un entier indiquant l'état actuel du systéme
     * @param result : une chaîne de caractére qui contient le resultat de reconnaissance
     * @param name : une chaîne de caractére qui contient le nom de la personne si elle est reconnue
     * @param cpu : une chaîne de caractére qui contient le temps de reconnaissance
     * @return Valeur booléenne indiquant succès/échec de la fonction
     */


    //bool DrawFaceDetectedRect(Mat *image, Rect *face_rect, DisplayInfo *display_info);

    //bool DrawCounter(Mat *image, DisplayInfo *display_info);

    //bool DrawMessage(Mat *image, DisplayInfo *display_info);

    //bool DrawRecognition(Mat *image, DisplayInfo *display_info);

//signals:

    /*! @brief la taille minimale de la longueur des VehicleBlobs (pour les vidéos ADM uniquement)
     *
     * @param frame : une image surlaquelle le resultat de la détection va être affiché
     */
    //void VehicleDisplayed(std::vector<Vehicle>* vehicles,Frame* frame);

private:

    /*! @brief tableau de 9 couleurs différentes pour dessiner les trajectoires de chaque véhicules suivis
     *
     */

    cv::Scalar colors_[9] = { cv::Scalar(255, 0, 0), cv::Scalar(0, 255, 0), cv::Scalar(0, 0, 255), cv::Scalar(255, 255, 0), cv::Scalar(0, 255, 255), cv::Scalar(255, 0, 255), cv::Scalar(255, 127, 255), cv::Scalar(127, 0, 255), cv::Scalar(127, 0, 127) };

};

#endif // Display_H
