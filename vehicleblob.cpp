#include "vehicleblob.h"

using namespace std;
using namespace cv;

VehicleBlob::VehicleBlob()
{


}

void VehicleBlob::SetScope (cv::Rect bbox)
{
    bbox_=bbox;
}


void VehicleBlob::SetCentroid(Point centroid)
{

    centroid_=centroid;
}

cv::Rect VehicleBlob::GetScope()
{
    return bbox_;
}

Point VehicleBlob::GetCentroid()
{
    return centroid_;
}

Frame *VehicleBlob::GetFrame()
{
    return frame_;
}

void VehicleBlob::SetFrame(Frame *frame)
{
    frame_ = frame;
}

