#include "vehicle.h"

using namespace std;
using namespace cv;

Vehicle::Vehicle()
{
    color_="";
    is_classified_ = false;
    license_plate_="";
}

void Vehicle::SetId(size_t id){
    id_ = id;
}

void Vehicle::SetVehicleBlobs(vector<VehicleBlob> blobs){
    blobs_ =  blobs;
}

void Vehicle::AddVehicleBlob(VehicleBlob blob){
    blobs_.push_back(blob);
}

void Vehicle::SetDistanceList(vector<float> distance_list){
    distance_list_ =  distance_list;
}

void Vehicle::AddDistance(float distance){
    distance_list_.push_back(distance);
}

void Vehicle::SetSpeedList(vector<float> speed_list){
    speed_list_ =  speed_list;
}

void Vehicle::AddSpeed(float speed){
    speed_list_.push_back(speed);
}

void Vehicle::SetColor(string color){
    color_ = color;
}
void Vehicle::SetClass(VehicleClass classe){
    class_ = classe;
    is_classified_ = true;
}

void Vehicle::SetLP(std::string license_plate){
    license_plate_ = license_plate;
}

void Vehicle::SetDepthImage(cv::Mat depth_image)
{
    depth_image_ = depth_image;
}

size_t Vehicle::GetId(){
    return id_;
}

std::vector<VehicleBlob> Vehicle::GetVehicleBlobs(){
    return blobs_;
}

VehicleBlob Vehicle::GetLastVehicleBlob(){
    if (!blobs_.empty())
        return blobs_[blobs_.size()-1];
    return VehicleBlob();
}

vector<float> Vehicle::GetDistanceList(){
    return distance_list_;
}

vector<float> Vehicle::GetSpeedList(){
    return speed_list_;
}

float Vehicle::GetCurrentDistance(){
    if (distance_list_.empty())
        return -1;
    return distance_list_[distance_list_.size()-1];
}

float Vehicle::GetCurrentSpeed(){
    if (speed_list_.empty())
        return -1;
    return speed_list_[speed_list_.size()-1];
}

string Vehicle::GetColor(){
    return color_;
}

VehicleClass Vehicle::GetClass(){
    return class_;
}

std::string Vehicle::GetLP(){
    return license_plate_;
}

cv::Mat Vehicle::GetDepthImage()
{
    return depth_image_;
}

bool Vehicle::IsClassified()
{
    return is_classified_;
}

bool Vehicle::IsRecognizedColor()
{
    return (color_.size()!=0);
}
bool Vehicle::IsRecognizedLP()
{
    return (license_plate_.size()!=0);
}
