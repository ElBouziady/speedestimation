#ifndef SPEEDESTIMATOR_H
#define SPEEDESTIMATOR_H

//#include <QObject>
#include "frame.h"
#include "vehicle.h"

#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/nonfree/nonfree.hpp"

/**
 * @file        speedestimator.h
 * @author      EL Bouziady Abderrahim
 * @version     1.0.0
 */

#ifndef CONFIG_SPEEDESTIMATION
#define CONFIG_SPEEDESTIMATION

struct ConfigSpeedEstimation{
    float baseline;
    float view_angle_camera;
    float pitch;
    float focal_distance;
    int image_width;
    float slope_min;
    float slope_max;
    int shift_x;
    int shift_y;
    int scope;
};

#endif

class SpeedEstimator //: public QObject
{
    //Q_OBJECT
public:
    /*explicit*/ SpeedEstimator(/*QObject *parent = 0*/);

    float ConvertDisparity2Distance(float disparity);

    bool ComputeDistanceStereo(Vehicle* vehicle);

    bool ComputeDistanceStereo(Vehicle* vehicle,Frame* frame);

    void SetConfig(ConfigSpeedEstimation config);

    bool SaveData(std::vector<Vehicle>* vehicles,Frame* frame);

    bool LoadData(std::vector<Vehicle>* vehicles,Frame* frame);

//signals:

  //  void SpeedEstimated(std::vector<Vehicle>* vehicles,Frame* frame);

//public slots:

    bool EstimateSpeed(std::vector<Vehicle>* vehicles,Frame* frame);

private:

    float baseline_;

    float view_angle_camera_;

    float pitch_ ;

    float focal_distance_;

    int image_width_;

    float slope_min_;

    float slope_max_;

    int shift_x_;

    int shift_y_;

    int scope_;

    int index_frame_;
};

#endif // SPEEDESTIMATOR_H
