#ifndef FRAME_H
#define FRAME_H

#define INVALID 0
#define ACQUIRE 1
#define PREPROCESS 2
#define DEPTH 3
#define DETECT 4
#define CLASSIFY 5
#define TRACK 6
#define RECOGNIZE 7
#define SPEED 8
#define DISPLAY 9

#include <opencv2/opencv.hpp>
#include <fstream>

//#include <QMutex>

#include "vehicleblob.h"

class VehicleBlob;

/**
 * @file        frame.h
 * @author      EL Bouziady Abderrahim
 * @version     1.0.0
 */

/*!
 * @ingroup     Buffer
 * @class       frame    frame.h
 * @brief Permet de stocker les informations d'un frame
 */

class Frame
{

public:

    /*!
     * @brief Constructeur par défaut de la classe Frame
     */

    Frame();

    /*!
     * @brief GetImageInput: recevoir une image mono
     * @return: une image
     */
    cv::Mat GetImageInputMono();

    /*!
     * @brief GetImagePreprocessed: recevoir l'image mono pre traitée
     * @return: l'image après la phase de pre traitement
     */
    cv::Mat GetImagePreprocessedMono();


    /*!
     * @brief GetImageInput: recevoir une image stereo
     * @return: une image
     */
    cv::Mat GetImageInputStereo();

    /*!
     * @brief GetImagePreprocessed: recevoir l'image stereo pre traitée
     * @return: l'image après la phase de pre traitement
     */
    cv::Mat GetImagePreprocessedStereo();

    /*!
     * @brief GetImageDepth: recevoir l'image de profondeur
     * @return: l'image après la phase de calcul de profondeur
     */
    cv::Mat GetImageDepth();

    /*!
     * @brief GetVehiclesDetected: recevoir les blobs des véhicules détectés
     * @return: un vecteur de blob des véhicules détectés
     */
    std::vector<VehicleBlob> GetVehiclesDetected();

    /*!
     * @brief GetImageDisplay: recevoir l'image à afficher
     * @return: une image
     */

    cv::Mat GetImageDisplay();

    /*!
     * @brief permet de verifier si l'image est vide sinon on effectue son acquisition

     @param image d'entrée
     */

    void SetImageInputMono(cv::Mat image_input_mono);

    /*!
     * @brief image passe par l étape de pré traitement

     @param image pré traité
     */
    void SetImagePreprocessedMono(cv::Mat image_preprocessed_mono);


    /*!
     * @brief permet de verifier si l'image est vide sinon on effectue son acquisition

     @param image d'entrée
     */

    void SetImageInputStereo(cv::Mat image_input_mono, cv::Mat image_input_stereo);

    /*!
     * @brief image passe par létaoe de pré traitement

     @param image pré traité
     */
    void SetImagePreprocessedStereo(cv::Mat image_preprocessed_mono, cv::Mat image_preprocessed_stereo);

    /*!
     * @brief image passe par l'etape de calcul de profondeur

     @param image de profondeur
     */
    void SetImageDepth(cv::Mat image_depth);

    /*!
     * @brief image contenant des véhicules

     @param vecteur de blob : des véhicules détectés
     */
    void SetVehiclesDetected(std::vector<VehicleBlob> vehicles_detected);

    /*!
     * @brief affichage

     @param image a affiché
     */
    void SetImageDisplay(cv::Mat image_display);

    /*!
     * @brief le thread prend le mutex
     * @param : nom du bloc courant

     */
    void Lock(std::string bloc_name);

    /*!
     * @brief le thread libère le mutex
     * @param : nom du bloc courant

     */
    void Unlock(std::string bloc_name);

    /*!
     * @brief verifier si le thread a libéré le mutex

     * @return: Boolean indiquant si le thread a libéré le mutex ou pas
     */
    bool IsLocked();

    /*!
     * @brief l'état de la validité de l'image

     * @return: Boolean indiquant si l'image est invalid ou pas
     */
    bool IsInvalid();

    /*!
     * @brief l'état de l'acquisition de l'image

     * @return: Boolean indiquant si l'etape a ete faite ou pas
     */
    bool IsAcquired();

    /*!
     * @brief l'état de l'étape pre traitraitement

     * @return: Boolean indiquant si l'etape a ete faite ou pas
     */
    bool IsPreprocessed();

    /*!
     * @brief l'état de l'étape generation de carte de profondeur

     * @return: Boolean indiquant si l'etape a ete faite ou pas
     */
    bool IsDepth();

    /*!
     * @brief l'état de l'étape detection

     * @return: Boolean indiquant si l'etape a ete faite ou pas
     */
    bool IsDetected();

    /*!
     * @brief l'état de l'étape de classification

     * @return: Boolean indiquant si l'etape a ete faite ou pas
     */
    bool IsClassified();

    /*!
     * @brief l'état de l'étape de suivi

     * @return: Boolean indiquant si l'etape a ete faite ou pas
     */
    bool IsTracked();

    /*!
     * @brief l'état de l'étape de reconnaissance

     * @return: Boolean indiquant si l'etape a ete faite ou pas
     */
    bool IsRecognized();

    /*!
     * @brief l'état de l'étape d'estimation de la vitesse

     * @return: Boolean indiquant si l'etape a ete faite ou pas
     */
    bool IsSpeed();

    /*!
     * @brief l'état de l'affichage

     * @return: Boolean indiquant si l'etape a ete faite ou pas
     */
    bool IsDisplayed();


    std::vector<cv::Mat> GetVehiclesDetectedDepth();

    void SetVehiclesDetectedDepth(std::vector<cv::Mat> &vehicles_detected_depth);

    bool IsStereo();

    void SetStereo(bool is_stereo);

    void SetState(int state);

    double GetTotalCPUTime();

private:

    /*!
     * @brief Mutex
     */
    //QMutex* mutex_;

    /*!
     * @brief image d'entrée
     */
    cv::Mat image_input_mono_;

    /*!
     * @brief image pré traitée
     *
     */
    cv::Mat image_preprocessed_mono_;

    /*!
     * @brief image d'entrée
     */
    cv::Mat image_input_stereo_;

    /*!
     * @brief image pré traitée
     *
     */
    cv::Mat image_preprocessed_stereo_;

    /*!
     * @brief carte de profondeur
     *
     */
    cv::Mat image_depth_;

    /*!
     * @brief vecteur de mat : les cartes de profondeurs des véhicules détectés
     */
    std::vector<cv::Mat> vehicles_detected_depth_;

    /*!
     * @brief image a affiché
     */
    cv::Mat image_display_;

    /*!
     * @brief vecteur de blob : les véhicules détectés
     */
    std::vector<VehicleBlob> vehicles_detected_;

    /*!
     * @brief l'état du mutex
     */
    int state_;

    /*!
     * @brief boolean verifiant si le thread a libéré le mutex
     */
    bool is_locked_;

    /*!
     * @brief boolean verifiant si le frame est stereo ou pas
     */
    bool is_stereo_;

    /*!
     * @brief entier indiquant le temps de depart de traitement d'un certain bloc
     */
    int64 start_time_;

    /*!
     * @brief entier indiquant le temps de fin de traitement d'un certain bloc
     */
    int64 end_time_;

    /*!
     * @brief reel indiquant le temps de traitement total du frame
     */
    double total_cpu_time_;
};

#endif // FRAME_H
